//
//  StringExtensionTests.swift
//  TravelStartTestTests
//
//  Created by yishain chen on 2018/2/15.
//  Copyright © 2018年 yishain chen. All rights reserved.
//

import XCTest
@testable import TravelStartTest

class StringExtensionTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSpliteImageUrlPathsString_WithValidImageFormat_ShouldReturnImageUrlPathsArray() {
        let imageUrlPathsString = "http://www.travel.taipei/d_upload_ttn/sceneadmin/image/A0/B0/C0/D11/E257/F345/546f42a1-cbfb-49fc-b4d8-c33f1c44190c.jpghttp://www.travel.taipei/d_upload_ttn/sceneadmin/image/A0/B0/C0/D31/E337/F270/85f03be7-cd0c-4bde-812f-7b7f603d7281.jpghttp://www.travel.taipei/d_upload_ttn/sceneadmin/image/A0/B0/C0/D5/E118/F202/2f1620b9-38b1-4c17-843b-5fed2a5ae31e.jpghttp://www.travel.taipei/d_upload_ttn/sceneadmin/image/A0/B0/C0/D7/E895/F87/f3eb1428-6152-4c15-b328-575d59e4477c.jpghttp://www.travel.taipei/d_upload_ttn/sceneadmin/image/A0/B0/C0/D12/E556/F480/9461764b-0049-480c-9f96-ca1c1fb2b9bc.jpghttp://www.travel.taipei/d_upload_ttn/sceneadmin/image/A0/B0/C0/D18/E762/F342/78245221-888e-44d0-ad0c-72a8e3494538.jpghttp://www.travel.taipei/d_upload_ttn/sceneadmin/image/A0/B0/C0/D22/E222/F927/dce66fe1-6277-422d-8537-e5cb8fe8aaa5.jpghttp://www.travel.taipei/d_upload_ttn/sceneadmin/image/A0/B0/C0/D17/E672/F642/f4201ef7-2137-447c-b832-c1d699142675.jpg"
        let imageUrlPathsArray = imageUrlPathsString.splitImageUrls()
        XCTAssertEqual(imageUrlPathsArray.count, 8)
    }
    
    func testSpliteImageUrlPathsString_WithInvalidImageFormat_ShouldReturnImageUrlPathsArray() {
        let imageUrlPathsString = "http://www.travel.taipei.comhttp://www.chinatime.taipei.com"
        let imageUrlPathsArray = imageUrlPathsString.splitImageUrls()
        XCTAssertEqual(imageUrlPathsArray.count, 0)
    }
}

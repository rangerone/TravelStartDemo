//
//  SceneInfoInteractorTests.swift
//  TravelStartTestTests
//
//  Created by yishain chen on 2018/2/16.
//  Copyright © 2018年 yishain chen. All rights reserved.
//

import XCTest
@testable import TravelStartTest
import RxSwift
import RxCocoa
import RxTest

class SceneInfoInteractorTests: XCTestCase {
    
    var interactor: SceneInfoInteractor!
    var sceneInfoDataManagerMock: SceneInfoDataManagerMock!
    
    var disposeBag: DisposeBag!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        self.disposeBag = DisposeBag()
        sceneInfoDataManagerMock = SceneInfoDataManagerMock()
        self.interactor = SceneInfoInteractor(sceneInfoDataManager: sceneInfoDataManagerMock)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        self.disposeBag = nil
        self.interactor = nil
        super.tearDown()
    }
    
    func testGetSceneInfoDataAtIndex0_ShouldReturnPageAtIndex0()  {
        sceneInfoDataManagerMock.getSceneInfoDataAtIndexShouldReturn = createSceneInfoData()
        let testScheduler = TestScheduler(initialClock: 0)
        let observer = testScheduler.createObserver([SceneData].self)
        self.interactor.getSceneInfoData(atIndex: 0, pageSize: 10)
            .drive(observer)
            .disposed(by: disposeBag)
        
        let nextEvent = observer.events.first!
        if let sceneInfoArray = nextEvent.value.element {
            XCTAssertTrue(sceneInfoArray.count == 2)
            XCTAssertFalse(sceneInfoArray.first?.stitle == "大安森林公園")
            XCTAssertTrue(sceneInfoArray.first?.stitle == "陽明山溫泉區")
        } else {
            XCTFail()
        }
        
        let completedEnvent = observer.events.last!
        XCTAssertTrue(completedEnvent.value.isCompleted)
        
    }
    
}

extension SceneInfoInteractorTests {
    private func createSceneInfoData() -> Driver<[SceneData]> {
        let sceneOne: SceneData = SceneData(id: "6",
                                            rowNumber: "6",
                                            description: "養生溫泉",
                                            serialNumber: "2011051800000057",
                                            memoTime: "各旅館營業時間不同",
                                            stitle: "陽明山溫泉區",
                                            content: "陽明山舊稱草山，因紀念明朝學者王陽明而改名為陽明山，它是臺北的都市後花園，位居近郊，是大家很方便就可以遠離塵囂，休閒的好去處。由於它位居火山地帶，特殊的火山地形及地質構造，造就了這一區的溫泉景觀。陽明山溫泉大致可分為4個區域：陽明山國家公園周邊、冷水坑、馬槽及火庚子坪，由於地熱運動頻繁，這4個區域溫泉所含的礦物成分都不同，使得其水溫、泉質與療效也大不相同。 陽明山位於大屯火山區，溫泉在陽明山風景區內，分前山溫泉和後山溫泉兩塊，有很多溫泉旅館，加上山上眾多風景區和美食，很適合兩天一夜的登山遊園泡湯之旅。",
                                            address: "臺北市  北投區竹子湖路1之20號",
                                            postDate: "2012/04/27",
                                            imageFile: "http://www.travel.taipei/d_upload_ttn/sceneadmin/pic/11000985.jpghttp://www.travel.taipei/d_upload_ttn/sceneadmin/pic/11000987.jpg",
                                            info: "捷運站名：捷運劍潭站，轉乘260、紅5公車至陽明山站。公車：109、260、260(區間車)、小9、小9(區間車)、紅5至陽明山站")
        
        let sceneTwo: SceneData = SceneData(id: "8",
                                            rowNumber: "8",
                                            description: "戶外踏青",
                                            serialNumber: "2011051800000057",
                                            memoTime: "各旅館營業時間不同",
                                            stitle: "大安森林公園",
                                            content: "大安森林公園佔地約26公頃，於民國八十三年三月二十九日啟園。雖位於台北市區中心，但卻是一個草木濃密的生態公園，以「都會森林」的型態成為台北市的都市之肺。公園內除有綠樹成蔭之外，更有花壇處處，每逢花開時節色彩繽紛，令人賞心悅目。公園外人行道則以綠樹與道路區隔，臨建國南路為盾柱木，臨和平東路為樟樹、臨新生南路為白千層、臨信義路則為榕樹，人行道中央還植有楓香樹，呈現多層式綠籬景觀。此外，各種休憩設施如行人座椅、涼亭、音樂舞台、慢跑道……等各種設施相當完善",
                                            address: "臺北市",
                                            postDate: "2012/05/17",
                                            imageFile: "http://www.travel.taipei/d_upload_ttn/sceneadmin/image/A0/B0/C0/D11/E257/F345/546f42a1-cbfb-49fc-b4d8-c33f1c44190c.jpghttp://www.travel.taipei/d_upload_ttn/sceneadmin/image/A0/B0/C0/D31/E337/F270/85f03be7-cd0c-4bde-812f-7b7f603d7281.jpghttp://www.travel.taipei/d_upload_ttn/sceneadmin/image/A0/B0/C0/D5/E118/F202/2f1620b9-38b1-4c17-843b-5fed2a5ae31e.jpg",
                                            info: "信義幹線、指南客運3號至「大安森林公園」站下車即達")
        
        
        let sceneInfoArray: [SceneData] = [sceneOne, sceneTwo]
        
        return Driver.just(sceneInfoArray)
    }
}

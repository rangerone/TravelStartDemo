//
//  SceneInfoDataManagerMock.swift
//  TravelStartTestTests
//
//  Created by yishain chen on 2018/2/16.
//  Copyright © 2018年 yishain chen. All rights reserved.
//

import Foundation
@testable import TravelStartTest
import RxCocoa

class SceneInfoDataManagerMock: SceneInfoDataManagerProtocol {
    var getSceneInfoDataIndex: UInt?
    var getSceneInfoDataPageSize: UInt?
    var getSceneInfoDataAtIndexWasCalled: Bool?
    var getSceneInfoDataAtIndexShouldReturn: Driver<[SceneData]>!
    
    func getSceneInfoData(atIndex index: UInt, pageSize: UInt) -> Driver<[SceneData]> {
        getSceneInfoDataIndex = index
        getSceneInfoDataPageSize = pageSize
        getSceneInfoDataAtIndexWasCalled = true
        return getSceneInfoDataAtIndexShouldReturn
    }
}

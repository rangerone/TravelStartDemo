//
//  SceneInfoDataManagerProtocol.swift
//  TravelStartTest
//
//  Created by yishain chen on 2018/2/16.
//  Copyright © 2018年 yishain chen. All rights reserved.
//

import Foundation
import RxCocoa

protocol SceneInfoDataManagerProtocol {
    func getSceneInfoData(atIndex index: UInt, pageSize: UInt) -> Driver<[SceneData]>
}

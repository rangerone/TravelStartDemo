//
//  SceneInfoDataManager.swift
//  TravelStartTest
//
//  Created by yishain chen on 2018/2/1.
//  Copyright © 2018年 yishain chen. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxAlamofire
import ObjectMapper
import SwiftyJSON
import Alamofire

class SceneInfoDataManager: NSObject, SceneInfoDataManagerProtocol {
    
    func getSceneInfoData(atIndex index: UInt, pageSize: UInt) -> Driver<[SceneData]> {
        let offset = index * pageSize
        let parameters = [
            "scope"  : "resourceAquire",
            "rid"    : "36847f3f-deff-4183-a5bb-800737591de5",
            "limit"  : pageSize.description,
            "offset" : offset.description
        ]
        return RxAlamofire
            .request(.get, Tourism_WebServiceAPI, parameters: parameters)
            .responseJSON()
            .catchError { error in
                NotificationCenter.default.post(name: Notification.Name(rawValue: Config.kNotificationConnectError), object: error)
                return Observable.error(error)
            }
            .retry()
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .filter({ $0.result.value != nil })
            .map({ (response) -> [SceneData] in
                let json = JSON(response.result.value!)
                let dataResponse = json["result"]["results"].object
                if let responseObject = Mapper<SceneData>().mapArray(JSONObject: dataResponse) {
                    return responseObject
                } else {
                    return []
                }
            })
            .observeOn(MainScheduler.instance) // switch to MainScheduler
            .asDriver(onErrorJustReturn: []) // This also makes sure that we are on MainScheduler
    }
}



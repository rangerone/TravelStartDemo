//
//  SceneInfoInteractor.swift
//  TravelStartTest
//
//  Created by yishain chen on 2018/2/1.
//  Copyright © 2018年 yishain chen. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SceneInfoInteractor: NSObject {

    var sceneInfoDataManager: SceneInfoDataManagerProtocol
    
    init(sceneInfoDataManager: SceneInfoDataManagerProtocol) {
        self.sceneInfoDataManager = sceneInfoDataManager
        super.init()
    }
    
    func getSceneInfoData(atIndex index: UInt, pageSize: UInt) -> Driver<[SceneData]> {
        return self.sceneInfoDataManager.getSceneInfoData(atIndex: index, pageSize: pageSize)
    }
}

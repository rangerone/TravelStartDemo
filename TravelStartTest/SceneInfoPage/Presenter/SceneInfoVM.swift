//
//  SceneInfoVM.swift
//  TravelStartTest
//
//  Created by yishain chen on 2018/2/1.
//  Copyright © 2018年 yishain chen. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SceneInfoVM: NSObject {

    private let pageSize: UInt = 10
    
    var sceneInfoInteractor: SceneInfoInteractor
    var sceneDataSource = Variable<[SceneData]>([])
    var pageIndex: UInt = 0
    var selectedCell = Variable<[SceneData]>([])
    var executing = Variable<Bool>(false)
    let disposeBag = DisposeBag()
    var hasNextPage = true

    /// 驅動page載入下個分頁
    let loadNextPageTrigger: PublishSubject<Void> = PublishSubject<Void>()
    
    private var page: Observable<[SceneData]> {
        get {
            func nextPage(_ previousPage: [SceneData]?) -> Observable<[SceneData]> {
                return sceneInfoInteractor.getSceneInfoData(atIndex: pageIndex, pageSize: pageSize).asObservable()
            }
            
            func hasNext(_ page: [SceneData]) -> Bool {
                pageIndex += 1
                return hasNextPage
            }
            
            if _page == nil {
                _page = Observable.page(make: nextPage, while: hasNext, when: loadNextPageTrigger)
            }
            return _page!
        }
    }
    
    var _page: Observable<[SceneData]>?
    
    struct Input {
        let loadMore: Driver<Void>
    }
    
    struct Output {
        let sceneDataSource: Driver<[SceneData]>
        let executing: Driver<Bool>
    }
    
    init(sceneInfoInteractor: SceneInfoInteractor) {
        self.sceneInfoInteractor = sceneInfoInteractor
        self.pageIndex = 0
        super.init()
    }
    
    func transform(input: Input) -> Output {
        
        input.loadMore.drive(loadNextPageTrigger)
            .disposed(by: disposeBag)

        self.setupNextPageObservables()
        
        return Output(sceneDataSource: sceneDataSource.asDriver(), executing: executing.asDriver())
    }
   
    func setupNextPageObservables() {
        let nextPage = page.share(replay: 1)
        
        let moreSceneData = nextPage.asDriver(onErrorRecover: { error in
            Driver.just([])
        })
        
        let allScenesLoaded = nextPage.materialize().filter { event -> Bool in
            switch event {
            case .completed:
                return true
            default:
                return false
            }
        }
        
        moreSceneData
            .do(onNext: { [unowned self] (moreSceneData) in
                self.hasNextPage = moreSceneData.count == self.pageSize
            })
            .map { moreSceneData in self.sceneDataSource.value + moreSceneData }
            .drive(self.sceneDataSource)
            .disposed(by: disposeBag)
        
        let executingObservables = [
            loadNextPageTrigger.asObservable().takeUntil(allScenesLoaded).map { _ in true },
            sceneDataSource.asObservable().map { _ in false }
        ]
        
        
        Observable.from(executingObservables)
            .merge()
            .startWith(true)
            .asDriver(onErrorDriveWith: Driver.just(false))
            .drive(self.executing)
            .disposed(by: disposeBag)
    }
    
}

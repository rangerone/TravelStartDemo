//
//  SceneWireFrame.swift
//  TravelStartTest
//
//  Created by yishain chen on 2018/2/12.
//  Copyright © 2018年 yishain chen. All rights reserved.
//

import UIKit

class SceneWireFrame: NSObject {
    
    @objc class func getSceneListVC() -> SceneListVC {
        let sceneInfoDataManager = SceneInfoDataManager()
        let sceneInfoInteractor = SceneInfoInteractor(sceneInfoDataManager: sceneInfoDataManager)
        let sceneInfoVM = SceneInfoVM(sceneInfoInteractor: sceneInfoInteractor)
        let sceneListVC = SceneListVC(sceneInfoVM: sceneInfoVM)
        return sceneListVC
    }
}

//
//  TourismWebServiceAPI.swift
//  TravelStartTest
//
//  Created by yishain chen on 2018/2/4.
//  Copyright © 2018年 yishain chen. All rights reserved.
//

import Foundation
import ObjectMapper

let Tourism_WebServiceAPI = "http://data.taipei/opendata/datalist/apiAccess?"

struct SceneData {
    var id: String?
    var rowNumber: String?
    var description: String?
    var serialNumber: String?
    var memoTime: String?
    var stitle: String?
    var content: String?
    var address: String?
    var postDate: String?
    var imageFile: String?
    var info: String?
}

extension SceneData : Mappable {
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        id <- map["_id"]
        rowNumber <- map["RowNumber"]
        description <- map["CAT2"]
        serialNumber <- map["SERIAL_NO"]
        memoTime <- map["MEMO_TIME"]
        stitle <- map["stitle"]
        content <- map["xbody"]
        address <- map["address"]
        postDate <- map["xpostDate"]
        imageFile <- map["file"]
        info <- map["info"]
    }
}



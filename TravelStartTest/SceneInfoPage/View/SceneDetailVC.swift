//
//  SceneDetailVC.swift
//  TravelStartTest
//
//  Created by yishain chen on 2018/2/10.
//  Copyright © 2018年 yishain chen. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

private let SceneImagesCollectionViewCellIdentifier = "SceneImagesCollectionViewCellIdentifier"

class SceneDetailVC: UIViewController {
    var imagesCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
    var sceneImageView = UIImageView()
    var sceneData : SceneData?
    var imageUrlsArray = [String]()
    var sceneNameTitle = UILabel()
    var sceneNameContent = UILabel()
    var separatorLine = UIView()
    var sceneDescriptionTitle = UILabel()
    var sceneDescriptionContent = UILabel()
    var sceneInfoScrollView = UIScrollView()
    
    var disposeBag = DisposeBag()
    
    init(sceneData: SceneData) {
        self.sceneData = sceneData
        super.init(nibName: nil, bundle: nil)
        self.setupLayout()
        self.configureCollectonView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.navigationItem.title = sceneData?.stitle
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupLayout() {
        
        view.addSubview(sceneInfoScrollView)
        sceneInfoScrollView.alwaysBounceVertical = true
        
        sceneNameTitle.text = "景點名稱"
        sceneNameTitle.textColor = UIColor.darkGray
        sceneNameTitle.font = UIFont.systemFont(ofSize: 12)
        
        sceneNameContent.text = sceneData?.stitle
        sceneNameTitle.textColor = UIColor.darkGray
        sceneNameContent.font = UIFont.systemFont(ofSize: 16)
        
        separatorLine.backgroundColor = UIColor.lightGray
        
        sceneDescriptionTitle.text = "景點介紹"
        sceneDescriptionTitle.textColor = UIColor.darkGray
        sceneDescriptionTitle.font = UIFont.systemFont(ofSize: 12)
        
        sceneDescriptionContent.text = sceneData?.content
        sceneDescriptionContent.textColor = UIColor.darkGray
        sceneDescriptionContent.numberOfLines = 0
        sceneDescriptionContent.font = UIFont.systemFont(ofSize: 14)
        
        sceneInfoScrollView.addSubview(imagesCollectionView)
        sceneInfoScrollView.addSubview(sceneNameTitle)
        sceneInfoScrollView.addSubview(sceneNameContent)
        sceneInfoScrollView.addSubview(separatorLine)
        sceneInfoScrollView.addSubview(sceneDescriptionTitle)
        sceneInfoScrollView.addSubview(sceneDescriptionContent)
        
        sceneInfoScrollView.snp.makeConstraints { make in
            make.edges.equalTo(view)
        }
        
        imagesCollectionView.snp.makeConstraints { make in
            make.left.right.equalTo(view)
            make.top.equalTo(sceneInfoScrollView.snp.top)
            make.height.equalTo(300)
        }
        
        sceneNameTitle.snp.makeConstraints { make in
            make.left.equalTo(view).offset(5)
            make.right.equalTo(view).offset(-5)
            make.top.equalTo(imagesCollectionView.snp.bottom).offset(10)
        }
        
        sceneNameContent.snp.makeConstraints { make in
            make.left.equalTo(view).offset(5)
            make.right.equalTo(view).offset(-5)
            make.top.equalTo(sceneNameTitle.snp.bottom).offset(3)
        }
        
        separatorLine.snp.makeConstraints { make in
            make.left.equalTo(view).offset(5)
            make.right.equalTo(view).offset(-5)
            make.height.equalTo(1)
            make.top.equalTo(sceneNameContent.snp.bottom).offset(10)
        }

        sceneDescriptionTitle.snp.makeConstraints { make in
            make.left.equalTo(view).offset(5)
            make.right.equalTo(view).offset(-5)
            make.top.equalTo(separatorLine.snp.bottom).offset(10)
        }

        sceneDescriptionContent.snp.makeConstraints { make in
            make.left.equalTo(view).offset(5)
            make.right.equalTo(view).offset(-5)
            make.top.equalTo(sceneDescriptionTitle.snp.bottom).offset(3)
            make.bottom.equalTo(sceneInfoScrollView.snp.bottom).offset(-10)
        }
        
    }
    
    func configureCollectonView() {
        imageUrlsArray = self.sceneData?.imageFile?.splitImageUrls() ?? []
        imagesCollectionView.register(ImagesCollectionViewCell.self, forCellWithReuseIdentifier: SceneImagesCollectionViewCellIdentifier)
        imagesCollectionView.backgroundColor = UIColor.white
        let cellSize = CGSize(width:self.view.frame.size.width, height:300)
        if let layout = imagesCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
            layout.itemSize = cellSize
            layout.minimumLineSpacing = 5.0
            imagesCollectionView.collectionViewLayout = layout
        }
        
        let imageDateSource = Driver.just(imageUrlsArray)
        imageDateSource.drive(imagesCollectionView.rx.items(cellIdentifier: SceneImagesCollectionViewCellIdentifier, cellType: ImagesCollectionViewCell.self)) { (row, elememt, cell) in
            cell.configure(withImageUrls: elememt)
        }.disposed(by: disposeBag)
    }

}

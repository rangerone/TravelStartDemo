//
//  ImagesCollectionViewCell.swift
//  TravelStartTest
//
//  Created by yishain chen on 2018/2/9.
//  Copyright © 2018年 yishain chen. All rights reserved.
//

import UIKit
import SDWebImage

class ImagesCollectionViewCell: UICollectionViewCell {
    
    var sceneImageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(sceneImageView)
        
        sceneImageView.snp.makeConstraints { make in
            make.top.bottom.right.left.equalTo(contentView)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(withImageUrls imageUrlString: String) {
        sceneImageView.sd_setImage(with: URL(string: "http" + imageUrlString), placeholderImage: UIImage(named: "placeholder.png"))
    }
    
    
}

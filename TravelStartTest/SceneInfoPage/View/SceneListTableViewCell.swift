//
//  SceneListTableViewCell.swift
//  TravelStartTest
//
//  Created by yishain chen on 2018/2/7.
//  Copyright © 2018年 yishain chen. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

private let SceneImagesCollectionViewCellIdentifier = "SceneImagesCollectionViewCellIdentifier"

class SceneListTableViewCell: UITableViewCell {

    var sceneTitle = UILabel()
    var sceneDescription = UILabel()
    var imagesCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
    var imageUrlsArray = [String]()
    var viewModel : SceneInfoVM?
    var sceneData : SceneData?
    
    var disposeBag = DisposeBag()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.configureCollectonView()
        self.selectionStyle = .none
        
        contentView.addSubview(sceneTitle)
        contentView.addSubview(sceneDescription)
        contentView.addSubview(imagesCollectionView)
        sceneTitle.snp.makeConstraints { make in
            make.top.equalTo(contentView.snp.top).offset(10)
            make.left.equalTo(contentView.snp.left).offset(10)
            make.right.equalTo(contentView.snp.right).offset(-10)
        }
        
        sceneDescription.snp.makeConstraints { make in
            make.top.equalTo(sceneTitle.snp.bottom).offset(10)
            make.left.equalTo(contentView.snp.left).offset(10)
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.bottom.equalTo(imagesCollectionView.snp.top).offset(-10)
        }
        
        imagesCollectionView.snp.makeConstraints { make in
            make.left.equalTo(contentView.snp.left).offset(10)
            make.right.equalTo(contentView.snp.right).offset(-10)
            make.height.equalTo(100)
            make.bottom.equalTo(contentView.snp.bottom).offset(-10)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configure(withSceneData sceneData:SceneData, viewModel:SceneInfoVM) {
        self.viewModel = viewModel
        self.sceneData = sceneData
        sceneTitle.font = UIFont.systemFont(ofSize: 16)
        
        sceneDescription.font = UIFont.systemFont(ofSize: 12)
        sceneDescription.numberOfLines = 0
        
        sceneTitle.text = sceneData.stitle
        sceneDescription.text = sceneData.content
        
        imageUrlsArray = sceneData.imageFile?.splitImageUrls() ?? []
        
        let imageDateSource = Driver.just(imageUrlsArray)
        imageDateSource.drive(imagesCollectionView.rx.items(cellIdentifier: SceneImagesCollectionViewCellIdentifier, cellType: ImagesCollectionViewCell.self)) { (row, elememt, cell) in
            cell.configure(withImageUrls: elememt)
            }.disposed(by: disposeBag)
        
        
        imagesCollectionView.rx.itemSelected.subscribe(onNext: { [unowned self] indexPath in
            if let sceneData = self.sceneData {
                self.viewModel?.selectedCell.value = [sceneData]
            }
        }).disposed(by: disposeBag)
    }
    
    func configureCollectonView() {
        imagesCollectionView.register(ImagesCollectionViewCell.self, forCellWithReuseIdentifier: SceneImagesCollectionViewCellIdentifier)
        imagesCollectionView.backgroundColor = UIColor.white
        let cellSize = CGSize(width:150, height:100)
        if let layout = imagesCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
            layout.itemSize = cellSize
            layout.minimumLineSpacing = 5.0
            imagesCollectionView.collectionViewLayout = layout
        }
        
    }
    
    override func prepareForReuse() {
        disposeBag = DisposeBag()
    }
}

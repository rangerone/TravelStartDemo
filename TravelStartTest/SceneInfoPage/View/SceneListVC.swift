//
//  SceneListVC.swift
//  TravelStartTest
//
//  Created by yishain chen on 2018/1/31.
//  Copyright © 2018年 yishain chen. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SnapKit

private let SceneListTableViewCellIdentifier = "SceneListTableViewCellIdentifier"

class SceneListVC: UIViewController {
    
    var activityIndicatorView = UIActivityIndicatorView()
    
    var sceneInfoVM: SceneInfoVM
    let sceneList = UITableView(frame: CGRect.zero)
    let disposeBag = DisposeBag()
    
    init(sceneInfoVM: SceneInfoVM) {
        self.sceneInfoVM = sceneInfoVM
        super.init(nibName: nil, bundle: nil)
        self.setupLayout()
        self.bindViewModel()
        self.configureTableView()
    }
    
    // pager trigger
    private lazy var loadNextPageTrigger: Observable<Void> = {
        return self.sceneList.rx.contentOffset
            .throttle(1, scheduler: MainScheduler.instance)
            .flatMap { [unowned self] (offset) -> Observable<Void> in
                return self.isNearTheBottomEdge(offset, self.sceneList)
                    ? Observable.just(Void())
                    : Observable.empty()
        }
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "台北市熱門景點"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func bindViewModel() {
        let loadMoreDriver = loadNextPageTrigger.asDriver(onErrorJustReturn: ())
        let input = SceneInfoVM.Input(loadMore: loadMoreDriver)
       
        let output = sceneInfoVM.transform(input: input)
        
        output.sceneDataSource
            .drive(sceneList.rx.items(cellIdentifier: SceneListTableViewCellIdentifier, cellType: SceneListTableViewCell.self)) {
                [unowned self] (row, elememt, cell) in
                cell.configure(withSceneData: elememt, viewModel: self.sceneInfoVM)
            }.disposed(by: disposeBag)
        
        output.executing
            .drive(activityIndicatorView.rx.isAnimating)
            .disposed(by: disposeBag)
        
        self.sceneInfoVM.selectedCell.asObservable()
            .filter({ $0.count > 0 })
            .subscribe(onNext: { [unowned self] sceneData in
                let sceneDetailVC = SceneDetailVC(sceneData: sceneData.first!)
                self.navigationItem.title = ""
                self.navigationController?.pushViewController(sceneDetailVC, animated: false)
        }).disposed(by: disposeBag)
        
    NotificationCenter.default.rx
        .notification(Notification.Name(Config.kNotificationConnectError))
        .takeUntil(self.rx.deallocated)
        .subscribe(onNext: { (error) in
            //show the error msg
            let alertController = UIAlertController(title: "網路錯誤", message: "目前無法抓取資料，請確認網路連線狀態。", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "確定", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }).disposed(by: disposeBag)
        
    }
    
    func setupLayout() {
        
        view.addSubview(sceneList)
        
        activityIndicatorView = UIActivityIndicatorView(frame: CGRect.zero)
        activityIndicatorView.activityIndicatorViewStyle = .gray
        view.addSubview(activityIndicatorView)
        
        sceneList.snp.makeConstraints { make in
            if #available(iOS 11.0, *) {
                make.top.equalTo(view.snp.topMargin)
            } else {
                make.top.equalTo(view.snp.topMargin).offset(64)
            }
            make.left.right.bottom.equalTo(view)
        }
        
        activityIndicatorView.snp.makeConstraints { make in
            make.bottom.equalTo(sceneList)
            make.left.right.equalTo(sceneList)
            make.height.equalTo(40)
        }
        
    }
    
    private func configureTableView() {
        sceneList.register(SceneListTableViewCell.self, forCellReuseIdentifier: SceneListTableViewCellIdentifier)
        sceneList.showsVerticalScrollIndicator = false
    }
    
    // MARK: Helper
    private func isNearTheBottomEdge(_ contentOffset: CGPoint, _ tableView: UITableView) -> Bool {
        return contentOffset.y +
            tableView.frame.size.height + 20 > tableView.contentSize.height
    }
}

//
//  StringExtension.swift
//  TravelStartTest
//
//  Created by yishain chen on 2018/2/14.
//  Copyright © 2018年 yishain chen. All rights reserved.
//

import Foundation

extension String {
    func splitImageUrls() -> [String] {
        let tempArray = self.components(separatedBy: "http")
        var imageUrlsArray = [String]()
        
        for element in tempArray {
            if element.contains("JPG") || element.contains("jpg") {
                imageUrlsArray.append(element)
            }
        }
        return imageUrlsArray
    }
}
